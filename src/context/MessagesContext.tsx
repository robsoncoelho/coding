import { createContext, useContext, useState } from "react";
import { MessagesContext, MessagesProvider } from "../utils/messages";

const Context = createContext<MessagesContext>({
  messages: [],
  isPaused: false,
  removeMessage: () => {},
  clearMessages: () => {},
  stopSubscriptions: () => {},
});

export const useMessages = () => useContext(Context);

export const Provider = ({
  children,
  messages,
  removeMessage,
  clearMessages,
  stopSubscriptions,
}: MessagesProvider) => {
  const [isPaused, setIsPaused] = useState<boolean>(false);

  return (
    <Context.Provider
      value={{
        messages: messages,
        isPaused: isPaused,
        clearMessages: clearMessages,
        removeMessage: removeMessage,
        stopSubscriptions: () => {
          stopSubscriptions();
          setIsPaused(!isPaused);
        },
      }}>
      {children}
    </Context.Provider>
  );
};
