
export interface MessagesContext {
  messages: Message[];
  isPaused?: boolean;
  clearMessages: () => void;
  removeMessage: (message: Message) => void;
  stopSubscriptions: () => void;
}

export interface MessagesProvider extends MessagesContext {
  children: React.ReactNode
}

export enum Priority {
  Error,
  Warn,
  Info,
}

export enum PriorityColors {
  Error = '#F56236',
  Warn = '#FCE788',
  Info = '#88FCA3',
}

export interface Message {
  message: string;
  priority: Priority;
}
