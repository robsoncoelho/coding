import Grid from "@material-ui/core/Grid";
import styled from "styled-components";
import { useMessages } from "../../context/MessagesContext";

import { Priority, Message } from "../../utils/messages";
import Column from "../column";

const GridComponent = styled(Grid)`
  && {
    margin-top: 50px;
  }
`;

const Component = (): JSX.Element => {
  const { messages } = useMessages();

  const getMessagesByPriority = (priority: Priority): Message[] =>
    messages?.filter?.((msg) => msg?.priority === priority);

  return (
    <GridComponent spacing={2} container>
      <Column
        title="Error Type 1"
        messages={getMessagesByPriority(Priority.Error)}
      />
      <Column
        title="Warning Type 2"
        messages={getMessagesByPriority(Priority.Warn)}
      />
      <Column
        title="Info Type 3"
        messages={getMessagesByPriority(Priority.Info)}
      />
    </GridComponent>
  );
};

export default Component;
