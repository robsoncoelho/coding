import styled from "styled-components";
import { Typography } from "@material-ui/core";
import { grey } from "@material-ui/core/colors";

const HeaderComponent = styled.div`
  border-bottom: 1px solid ${grey[500]};
  padding-bottom: 5px;
  margin-bottom: 5px;
`;

const Component = (): JSX.Element => {
  return (
    <HeaderComponent>
      <Typography variant="h5" component="h1">
        nuffsaid.com Coding Challenge
      </Typography>
    </HeaderComponent>
  );
};

export default Component;
