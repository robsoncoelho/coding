import { useEffect, useState } from "react";
import styled from "styled-components";
import { Snackbar } from "@material-ui/core";

import { PriorityColors, Priority } from "../../utils/messages";
import { usePrevious } from "../../utils/hooks";
import { useMessages } from "../../context/MessagesContext";

const SnackbarComponent = styled(Snackbar)`
  && > div {
    background-color: ${PriorityColors.Error};
    color: initial;
  }
`;

const Component = (): JSX.Element | null => {
  const { messages } = useMessages();
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>();

  const prevMessages = usePrevious(messages);

  useEffect(() => {
    if (messages.length > prevMessages?.length) {
      const latestMessage = messages[0];

      // GET ERROR MESSAGE
      if (latestMessage?.priority === Priority.Error) {
        setMessage(latestMessage?.message);
      }
    }
  }, [messages, prevMessages]);

  useEffect(() => {
    if (message) {
      setOpen(true);
      let snackbarTimer = setTimeout(() => setOpen(false), 2000);

      return () => {
        clearInterval(snackbarTimer);
      };
    }
  }, [message]);

  if (!open) return null;

  return (
    <SnackbarComponent
      open={true}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={() => setOpen(false)}
      message={message}
    />
  );
};

export default Component;
