import styled from "styled-components";
import { Button } from "@material-ui/core";
import { useMessages } from "../../context/MessagesContext";
import { PriorityColors } from "../../utils/messages";

const ButtonGroup = styled.div`
  text-align: center;
`;

const ButtonComponent = styled(Button)`
  && {
    background-color: ${PriorityColors.Info};
    margin: 0 2px;
    font-weight: bold;
  }
`;

const Component = (): JSX.Element => {
  const { clearMessages, stopSubscriptions, isPaused } = useMessages();

  return (
    <ButtonGroup>
      <ButtonComponent
        data-testid="startStopButton"
        variant="contained"
        onClick={() => stopSubscriptions()}>
        {!isPaused ? "Stop" : "Start"}
      </ButtonComponent>
      <ButtonComponent
        data-testid="clearButton"
        variant="contained"
        onClick={() => clearMessages()}>
        Clear
      </ButtonComponent>
    </ButtonGroup>
  );
};

export default Component;
