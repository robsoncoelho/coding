import styled from "styled-components";
import { Grid, Typography } from "@material-ui/core";
import { Message } from "../../utils/messages";
import Card from "../card";

type Props = {
  messages: Message[];
  title: string;
};

const Title = styled(Typography)`
  && {
    font-weight: bold;
  }
`;

const Counter = styled(Typography)`
  && {
    margin-bottom: 10px;
  }
`;

const Component = ({ messages, title }: Props): JSX.Element => {
  return (
    <Grid item xs={4} data-testid="column">
      <Title variant="h5">{title}</Title>
      <Counter>Count {messages?.length}</Counter>
      {messages?.map?.((msg) => (
        <Card message={msg} key={msg?.message} />
      ))}
    </Grid>
  );
};

export default Component;
