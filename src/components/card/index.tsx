import styled from "styled-components";
import {
  Card,
  Button,
  CardActions,
  CardContent,
  Typography,
} from "@material-ui/core";

import { useMessages } from "../../context/MessagesContext";
import { Message, Priority, PriorityColors } from "../../utils/messages";

type Props = {
  message: Message;
};

const CardComponent = styled(Card)<{ color: string }>`
  && {
    background-color: ${(props) => props.color};
    margin-bottom: 10px;
  }
`;

const CardContentComponent = styled(CardContent)`
  && {
    padding-bottom: 5px;
  }
`;

const CardActionsComponent = styled(CardActions)`
  && {
    justify-content: flex-end;
    padding-top: 0;
  }
`;

const ClearButtonComponent = styled(Button)`
  && {
    text-transform: none;
  }
`;

const Component = ({ message }: Props): JSX.Element => {
  const { removeMessage } = useMessages();

  const getPriorityColor = (priority: Priority): string => {
    const colors = [
      PriorityColors.Error,
      PriorityColors.Warn,
      PriorityColors.Info,
    ];

    return colors[priority];
  };

  return (
    <CardComponent
      data-testid="card"
      color={getPriorityColor(message?.priority)}>
      <CardContentComponent>
        <Typography>{message?.message}</Typography>
      </CardContentComponent>
      <CardActionsComponent>
        <ClearButtonComponent onClick={() => removeMessage(message)}>
          Clear
        </ClearButtonComponent>
      </CardActionsComponent>
    </CardComponent>
  );
};

export default Component;
