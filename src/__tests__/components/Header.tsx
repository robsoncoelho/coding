import renderWithContext from "../utils/context";
import Header from "../../components/header";

test("Renders Header with react context", () => {
  const comp = renderWithContext({ component: <Header /> });
  expect(comp).toBeTruthy();
});

test("Shows header component", () => {
  const comp = renderWithContext({ component: <Header /> });

  expect(comp.getByText("nuffsaid.com Coding Challenge")).toBeTruthy();
});
