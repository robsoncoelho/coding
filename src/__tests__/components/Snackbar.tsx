import renderWithContext from "../utils/context";
import Snackbar from "../../components/snackbar";

test("Renders Snackbar with react context", () => {
  const messages = [{ message: "Vel ut dolore vitae ratione 1.", priority: 0 }];
  const comp = renderWithContext({ component: <Snackbar />, messages });
  expect(comp).toBeTruthy();
});
