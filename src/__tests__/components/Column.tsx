import renderWithContext from "../utils/context";
import Column from "../../components/column";

const messages = [
  { message: "Vel ut dolore vitae ratione 1.", priority: 0 },
  { message: "Vel ut dolore vitae ratione 2.", priority: 0 },
];

test("Renders Column with react context", () => {
  const comp = renderWithContext({
    component: <Column messages={messages} title="Error Type 1" />,
  });
  expect(comp).toBeTruthy();
});

test("Shows title", () => {
  const comp = renderWithContext({
    component: <Column messages={messages} title="Error Type 1" />,
  });

  expect(comp.getByText("Error Type 1")).toBeTruthy();
});

test("Shows counter", () => {
  const comp = renderWithContext({
    component: <Column messages={messages} title="Error Type 1" />,
  });

  expect(comp.getByText("Count 2")).toBeTruthy();
});

test("Shows cards for each message", () => {
  const comp = renderWithContext({
    component: <Column messages={messages} title="Error Type 1" />,
  });

  expect(comp.getAllByTestId("card")).toHaveLength(2);
});
