import renderWithContext from "../utils/context";
import Card from "../../components/card";

const message = { message: "Vel ut dolore vitae ratione 1.", priority: 0 };

test("Renders Card with react context", () => {
  const comp = renderWithContext({ component: <Card message={message} /> });
  expect(comp).toBeTruthy();
});

test("Shows message", () => {
  const comp = renderWithContext({ component: <Card message={message} /> });

  expect(comp.getByText("Vel ut dolore vitae ratione 1.")).toBeTruthy();
});

test('Shows "Clear" button', () => {
  const comp = renderWithContext({ component: <Card message={message} /> });

  expect(comp.getByRole("button", { name: "Clear" })).toBeTruthy();
});
