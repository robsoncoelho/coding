import renderWithContext from "../utils/context";
import Table from "../../components/table";

test("Renders Table with react context", () => {
  const comp = renderWithContext({ component: <Table /> });
  expect(comp).toBeTruthy();
});

test("Shows priority columns", () => {
  const comp = renderWithContext({ component: <Table /> });

  expect(comp.getAllByTestId("column")).toHaveLength(3);
});

test("Loads messages separated by priority columns", () => {
  const messages = [
    { message: "Vel ut dolore vitae ratione.", priority: 0 },
    { message: "Vel ut dolore vitae ratione.", priority: 1 },
    { message: "Vel ut dolore vitae ratione.", priority: 2 },
  ];

  const comp = renderWithContext({ component: <Table />, messages });

  expect(comp.getAllByText("Count 1")).toHaveLength(3);
  expect(comp.getAllByText("Vel ut dolore vitae ratione.")).toHaveLength(3);
});
