import { fireEvent } from "@testing-library/react";
import renderWithContext from "../utils/context";
import ActionButtons from "../../components/actionButtons";

test("Renders ActionButtons with react context", () => {
  const comp = renderWithContext({ component: <ActionButtons /> });
  expect(comp).toBeTruthy();
});

test("Shows action buttons", () => {
  const comp = renderWithContext({ component: <ActionButtons /> });

  expect(comp.getByTestId("startStopButton")).toBeTruthy();
  expect(comp.getByTestId("clearButton")).toBeTruthy();
});

test('Changes button text by clicking on "StartStop" button', () => {
  const comp = renderWithContext({ component: <ActionButtons /> });

  expect(comp.getByTestId("startStopButton")).toHaveTextContent("Stop");

  fireEvent.click(comp.getByTestId("startStopButton"));

  expect(comp.getByTestId("startStopButton")).toHaveTextContent("Start");
});
