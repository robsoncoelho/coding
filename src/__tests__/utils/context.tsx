import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { Provider } from "../../context/MessagesContext";
import { Message } from "../../utils/messages";

type MessagesContext = {
  messages?: Message[];
  isPaused?: boolean;
  component: React.ReactNode;
  clearMessages?: () => void;
  removeMessage?: (message: Message) => void;
  stopSubscriptions?: () => void;
};

const renderWithContext = ({
  component,
  messages,
  removeMessage,
  clearMessages,
  isPaused,
  stopSubscriptions,
}: MessagesContext) => {
  const defaultProps = {
    messages: messages || [],
    isPaused: isPaused,
    removeMessage: () => removeMessage || {},
    clearMessages: () => clearMessages || {},
    stopSubscriptions: () => stopSubscriptions || {},
    children: component,
  };

  return render(
    <Provider {...defaultProps}>{defaultProps?.children}</Provider>
  );
};

export default renderWithContext;
