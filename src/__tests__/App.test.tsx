import { render } from "@testing-library/react";
import App from "../App";

test("Renders App with react context", () => {
  const comp = render(<App />);
  expect(comp).toBeTruthy();
});
