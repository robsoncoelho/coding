import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";

import generateMessage from "./Api";
import { Message } from "./utils/messages";
import { Provider as MessagesProvider } from "./context/MessagesContext";
import Header from "./components/header";
import Table from "./components/table";
import Snackbar from "./components/snackbar";
import ActionButtons from "./components/actionButtons";

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [stopSubscriptions, setStopSubscriptions] = useState<boolean>(false);

  useEffect(() => {
    if (!stopSubscriptions) {
      const cleanUp = generateMessage((message: Message) => {
        setMessages((oldMessages) => [message, ...oldMessages]);
      });
      return cleanUp;
    }
  }, [setMessages, stopSubscriptions]);

  const onRemoveMessage = (message: Message): void => {
    setMessages(messages.filter((item) => item !== message));
  };

  return (
    <MessagesProvider
      messages={messages}
      removeMessage={(message) => onRemoveMessage(message)}
      clearMessages={() => setMessages([])}
      stopSubscriptions={() => setStopSubscriptions(!stopSubscriptions)}>
      <Snackbar />
      <Header />
      <Container maxWidth="lg">
        <ActionButtons />
        <Table />
      </Container>
    </MessagesProvider>
  );
};

export default App;
